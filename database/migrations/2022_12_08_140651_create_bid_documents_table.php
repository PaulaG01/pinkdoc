<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bid_documents', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('bid_id')->nullable();
            $table->foreign('bid_id')->references('id')->on('bids');

            $table->string('company')->nullable();
            $table->string('event_type')->nullable();
            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->string('address')->nullable();
            $table->integer('guests_count')->nullable();
            $table->integer('budget')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bid_documents');
    }
}
