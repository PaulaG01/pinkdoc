<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bids', function (Blueprint $table) {
            $table->id();
            $table->string('owner');
            $table->string('phone');
            $table->string('email');

            $table->string('title');
            $table->text('text');
            $table->boolean('approved')->default(false);
            $table->timestamp('approved_date')->nullable();
            $table->boolean('done')->default(false);
            $table->timestamp('done_date')->nullable();

            $table->unsignedBigInteger('responsible')->nullable();
            $table->foreign('responsible')->references('id')->on('users');
            $table->timestamp('due_date')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bids');
    }
}
