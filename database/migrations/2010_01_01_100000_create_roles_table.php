<?php

use App\Models\Enums\RolesEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        DB::table('roles')->insert([
            ['id' => RolesEnum::ADMIN, 'name' => 'Администратор'],
            ['id' => RolesEnum::MANAGER, 'name' => 'Менеджер'],
            ['id' => RolesEnum::CHIEF, 'name' => 'Глава отдела'],
            ['id' => RolesEnum::DIRECTOR, 'name' => 'Директор'],
            ['id' => RolesEnum::CLIENT, 'name' => 'Клиент'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
