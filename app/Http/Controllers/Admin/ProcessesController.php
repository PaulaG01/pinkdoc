<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bid;
use Illuminate\Http\Request;

class ProcessesController extends Controller
{
    public function index()
    {
        $bids = Bid::all();

        return view('admin/processes/index', ['bids' => $bids]);
    }
}
