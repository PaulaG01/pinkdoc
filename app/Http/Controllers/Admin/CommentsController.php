<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DocumentComment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function create(Request $request)
    {
        DocumentComment::create([
            'text' => $request->text,
            'user_id' => auth()->user()->id,
            'document_id' => $request->document_id
        ]);

        return redirect(route('documents.show', ['id' => $request->document_id]));
    }
}
