<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DocumentApprover;
use Illuminate\Http\Request;

class DocumentApproversController extends Controller
{
    public function create(Request $request)
    {
        DocumentApprover::create([
            'user_id' => $request->userId,
            'document_id' => $request->documentId,
        ]);

        return redirect(route('documents.show', ['id' => $request->documentId]));
    }

    public function delete(Request $request)
    {
        $documentId = DocumentApprover::find($request->approveId)->document_id;

        DocumentApprover::where('id', $request->approveId)->delete();

        return redirect(route('documents.show', ['id' => $documentId]));
    }

    public function changeStatus(Request $request)
    {
        $documentId = DocumentApprover::find($request->approveId)->document_id;

        DocumentApprover::where('id', $request->approveId)->update(['approved' => $request->status]);

        return redirect(route('documents.show', ['id' => $documentId]));
    }
}
