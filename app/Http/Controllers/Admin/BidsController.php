<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Bid;
use App\Models\Enums\RolesEnum;
use App\Models\User;
use Illuminate\Http\Request;

class BidsController extends Controller
{
    public function index(Request $request)
    {
        $bidsQuery = Bid::where('owner', 'LIKE', "%{$request->search}%");

        if ($request->responsible) {
            $bidsQuery->where('responsible', $request->responsible);
        }

        $bids = $bidsQuery->get()->sortDesc();

        $managers = User::whereHas('role', function ($role) {
            $role->where('id', RolesEnum::MANAGER);
        })->get();

        return view('admin.bids.index', ['pageTitle' => 'Заявки', 'bids' => $bids, 'managers' => $managers]);
    }

    public function showCreateForm()
    {
        return view('admin.bids.create', ['pageTitle' => 'Создание заявки']);
    }

    public function my()
    {
        $bids = Bid::whereHas('responsibleUser', function ($user) {
            $user->where('id', auth()->user()->id);
        })->get()->sortDesc();

        return view('admin.bids.index', ['pageTitle' => 'Мои заявки', 'bids' => $bids]);
    }

    public function show(int $id)
    {
        $bid = Bid::find($id);

        if (!$bid) {
            abort(404);
        }

        $users = User::whereHas('role', function ($role) {
            $role->where('id', RolesEnum::MANAGER);
        })->get();

        return view('admin.bids.show', ['pageTitle' => "Заявка: $bid->title", 'bid' => $bid, 'users' => $users]);
    }

    public function update(Request $request)
    {
        $bid = Bid::find($request->id);

        if (!$bid) {
            abort(404);
        }

        $formData = $request->all();
        unset($formData['_token']);

        if (isset($formData['approved'])) {
            $formData['approved'] = 1;
        }

        if (isset($formData['done'])) {
            $formData['done'] = 1;
        }

       if (isset($formData['responsible']) && $formData['responsible'] == 0) {
           unset($formData['responsible']);
       }

        Bid::find($request->id)->update($formData);

        $bid = Bid::find($request->id);

        return redirect(route('bids.show', $bid->id));
    }

    public function create(Request $request)
    {
        Bid::create([
            'owner' => $request->owner,
            'title' => $request->title,
            'phone' => $request->phone,
            'email' => $request->email,
            'text' => $request->text,
        ]);

        if (auth()->user()){
            return redirect(route('bids.all'));
        }

        return redirect('/');
    }
}
