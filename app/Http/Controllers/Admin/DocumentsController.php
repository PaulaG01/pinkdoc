<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\PdfHelper;
use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class DocumentsController extends Controller
{
    public function index()
    {
        $documents = Document::orderByDesc('id')->groupBy('documentable_type')->groupBy('documentable_id')->get();

        return view('admin.documents.index', ['pageTitle' => 'Документы', 'documents' => $documents]);
    }

    public function show(int $id)
    {
        $document = Document::find($id);

        $entity = $document->documentable;

        $allDocuments = $entity->documents;

        $users = User::all();

        return view('admin.documents.show', ['pageTitle' => "История документа #$document->id", 'documents' => $allDocuments, 'users' => $users]);
    }

    public function renderPdf(Request $request)
    {
        $pdfcontent = PdfHelper::getPdfContent($request->pdf);

        return Response::make($pdfcontent, 200, ['content-type'=>'application/pdf']);
    }

    public function setActive(Request $request)
    {
        $document = Document::find($request->id);

        Document::where('documentable_type', $document->documentable_type)
            ->where('documentable_id', $document->documentable_id)
            ->update(['active' => 0]);

        $document->active = 1;
        $document->save();

        return redirect(route('documents.show', ['id' => $document->id]));
    }
}
