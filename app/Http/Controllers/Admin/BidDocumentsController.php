<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BidDocument;
use App\Models\BidDocumentItem;
use Illuminate\Http\Request;

class BidDocumentsController extends Controller
{
    public function update(Request $request)
    {
        $bid = BidDocument::find($request->id);

        if (!$bid) {
            abort(404);
        }

        $formData = $request->all();
        unset($formData['_token']);

        if (isset($formData['items'])) {
            foreach ($formData['items'] as $id => $item) {
                if (is_null($item)) {
                    $document = BidDocumentItem::find($id);
                    if ($document){
                        $document->delete();
                    }
                    continue;
                }
                if (BidDocumentItem::find($id)) {
                    BidDocumentItem::find($id)->update(['text' => $item]);
                    continue;
                }
                BidDocumentItem::create(['bid_document_id' => $bid->id, 'text' => $item]);
            }
            unset($formData['items']);
        }

        BidDocument::find($request->id)->update($formData);

        $bidDocument = BidDocument::find($request->id);

        return redirect(route('bids.show', $bidDocument->bid_id));
    }
}
