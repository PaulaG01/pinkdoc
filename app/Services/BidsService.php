<?php

namespace App\Services;

use App\Helpers\PdfHelper;
use App\Models\Bid;
use App\Models\BidDocument;
use App\Models\Document;
use App\Models\DTO\PDF\BidDocumentDTO;

class BidsService
{
    public static function getBidsCount(string $type = 'all')
    {
        switch ($type){
            case 'new':
                $count = Bid::where('approved', 0)->get()->count();
                break;
            default:
                $count = Bid::all()->count();
                break;
        }

        return $count;
    }

    public static function generateBidDocumentPdf($bidDocumentId)
    {
        $bidDocument = BidDocument::find($bidDocumentId);

        if (!$bidDocument) {
            abort(404);
        }

        Document::create(['documentable_type' => BidDocument::class, 'documentable_id' => $bidDocumentId, 'file' => 'tmp']);

        $document = Document::where('documentable_type', BidDocument::class)
            ->where('documentable_id', $bidDocumentId)->orderBy('id', 'DESC')->first();

        $data = BidDocumentDTO::transform(
            $bidDocument->company,
            $bidDocument->event_type,
            $bidDocument->address,
            $bidDocument->start_date,
            $bidDocument->end_date,
            $bidDocument->guests_count,
            $bidDocument->budget,
            $document->id,
            (array)$bidDocument->items
        );

        $file = PdfHelper::generatePdf('documents.bid', $data);

        $document->file = $file;
        $document->save();
    }
}
