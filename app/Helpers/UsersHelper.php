<?php

namespace App\Helpers;

use App\Models\User;

class UsersHelper
{
    /**
     * @param User $user
     * @return bool
     */
    public static function isCanBeDeleted(User $user): bool
    {
        $isCurrentUser = auth()->user()->id === $user->id;

        return !$isCurrentUser;
    }
}
