<?php

namespace App\Helpers;

use App\Models\DTO\IDTOTransformer;
use \Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Client\Request;

class PdfHelper
{
    /**
     * @param string $view
     * @param IDTOTransformer $dto
     */
    public static function generatePdf(string $view, IDTOTransformer $dto)
    {
        Pdf::setOption('defaultFont', 'Times');
        $pdf = Pdf::loadView($view, ['dto' => (array) $dto]);

        $pdfName = 'documents/'.self::getPdfName('organizacia_meropriyatia').'.pdf';

        $file = $pdf->output();
        file_put_contents("$pdfName", $file);

        return $pdfName;
    }

    public static function getPdfContent($pdfName)
    {
        return file_get_contents($pdfName);
    }

    /**
     * @param string $documentName
     * @return string
     */
    private static function getPdfName(string $documentName): string
    {
        return sprintf('%s_%s',
            date('d_m_Y_h_m_s'),
            $documentName
        );
    }
}
