<?php

namespace App\Models;

use App\Services\BidsService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

class BidDocument extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::saved(function(BidDocument $bidDocument) {
            if ($bidDocument->documents->count()) {
                BidsService::generateBidDocumentPdf($bidDocument->id);
            }
        });
    }

    public function bid(): BelongsTo
    {
        return $this->belongsTo(Bid::class);
    }

    public function items(): HasMany
    {
        return $this->hasMany(BidDocumentItem::class);
    }

    public function documents(): MorphMany
    {
        return $this->morphMany(Document::class, 'documentable')->orderBy('id', 'DESC');
    }

    public function getVersionsCountAttribute(): int
    {
        return $this->documents()->count();
    }

    public function getResponsibleUserAttribute()
    {
        return $this->bid->responsibleUser;
    }

    public function getSourceAttribute()
    {
        return $this->bid;
    }

    public function getSourceRouteAttribute()
    {
        return route('bids.show', ['id' => $this->bid->id]);
    }

}
