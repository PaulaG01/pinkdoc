<?php

namespace App\Models\Enums;

class ApproveStatusesEnum
{
    public const STATUSES = [
        1 => 'Принято',
        2 => 'Отклонено',
        3 => 'Ожидается',
    ];

    /** @var int */
    public const YES = 1;

    /** @var int */
    public const NO = 2;

    /** @var int */
    public const WAITING = 3;
}
