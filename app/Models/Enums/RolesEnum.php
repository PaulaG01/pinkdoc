<?php

namespace App\Models\Enums;

class RolesEnum
{
    /** @var int */
    /**
     * Site admin. Content creator
     */
    public const ADMIN = 1;

    /** @var int */
    /**
     * Rules bids, creates bids documents and set organizers
     */
    public const MANAGER = 2;

    /** @var int */
    /**
     * Documents statistics
     */
    public const CHIEF = 3;

    /** @var int */
    /**
     * All permissions
     */
    public const DIRECTOR = 4;

    /** @var int */
    /**
     * All permissions
     */
    public const CLIENT = 5;
}
