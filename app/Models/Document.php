<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $guarded = [];

    public function documentable()
    {
        return $this->morphTo();
    }

    public function comments()
    {
        return $this->hasMany(DocumentComment::class)->orderByDesc('id');
    }

    public function approves()
    {
        return $this->hasMany(DocumentApprover::class);
    }

    public function getTemplateAttribute()
    {
        switch ($this->documentable_type) {
            case BidDocument::class:
                $template = 'Заявка на организацию мероприятия';
                break;
            default:
                $template = '-';
        }

        return $template;
    }

    public function getIsApprovedAttribute(): int
    {
        return 0;
    }
}
