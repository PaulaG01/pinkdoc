<?php

namespace App\Models\DTO;

interface IDTOTransformer
{
    public static function transform(...$args): self;
}
