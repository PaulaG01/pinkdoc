<?php

namespace App\Models\DTO\PDF;

use App\Models\DTO\IDTOTransformer;

class BidDocumentDTO implements IDTOTransformer
{
    public string $company;
    public string $type;
    public string $address;
    public string $dateStart;
    public string $dateEnd;
    public int $guestsCount;
    public int $budget;
    public int $id;
    public array $items;

    public string $ownerCompanyTitle;
    public string $ownerCompanyAddress;
    public string $ownerCompanyDirector;

    public static function transform(...$args): self
    {
        $dto = new self();
        $dto->company = $args[0];
        $dto->type = $args[1];
        $dto->address = $args[2];
        $dto->dateStart = $args[3];
        $dto->dateEnd = $args[4];
        $dto->guestsCount = $args[5];
        $dto->budget = $args[6];
        $dto->id = $args[7];
        $dto->items = $args[8];

        //TODO: add editing in settings
        $dto->ownerCompanyTitle = 'ООО "ПинкДок"';
        $dto->ownerCompanyAddress = 'Париж, ул. Круассана 32А';
        $dto->ownerCompanyDirector = 'Печенюшкин Андрей Альбертович';

        return $dto;
    }
}
