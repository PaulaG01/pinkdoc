<?php

namespace App\Models;

use App\Services\BidsService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Nette\Utils\DateTime;

class Bid extends Model
{
    protected $guarded = [];

    public function setApprovedAttribute($approved)
    {
        $this->attributes['approved_date'] = now();
        $this->attributes['due_date'] = Carbon::today()->addDays(7);
        $this->document()->create();
        $this->attributes['approved'] = $approved;
    }

    public function setDoneAttribute($done)
    {
        $this->attributes['done_date'] = now();
        $this->attributes['done'] = $done;
        BidsService::generateBidDocumentPdf($this->document->id);
    }

    public function responsibleUser(): BelongsTo
    {
        return $this->belongsTo(User::class, 'responsible', 'id');
    }

    public function document(): HasOne
    {
        return $this->hasOne(BidDocument::class);
    }

    public function getDaysToPostponedAttribute(): int
    {
        $today = new DateTime();
        $dueDate = new DateTime($this->due_date);

        return $dueDate->diff($today)->days;
    }
}
