@extends('layouts.app')

@section('content')
    @if($documents->where('active', 1)->count())
        <div class="mt-3 generate">
            <div class="alert alert-warning" role="alert">
                Существует активная версия документа. Вы можете
                <a href="#">генерировать финальную версию документа</a> и завершить работу над этим документом.
            </div>
        </div>
    @endif
    <div class="history mt-3">
        <h4>Версии</h4>
        <table class="table table-success table-striped">
            <thead>
            <tr>
                <th scope="col">№ версии</th>
                <th scope="col">Дата</th>
                <th scope="col">Автор</th>
                <th scope="col">Документ</th>
                <th scope="col">Комментарии</th>
                <th scope="col">Активная версия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($documents as $index => $document)
                <tr>
                    <td>{{ $documents->count() - $index }}</td>
                    <td>{{ $document->created_at }}</td>
                    <td>{{ $document->documentable->responsibleUser->name }}</td>
                    <td><a href="#{{ $document->id }}">{{ $document->file }}</a></td>
                    <td>{{ $document->comments->count() }}</td>
                    <td>{{ $document->active ? 'Да' : '-' }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    @foreach($documents as $document)
        <hr>
        <div class="d-flex align-items-center">
            <h5 id="{{ $document->id }}">Документ №{{ $document->id }}</h5>
            @if($document->active)
                <div class="btn-success" style="margin-left: 5px;">Активен</div>
            @else
                <form action="{{ route('documents.setActive') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{ $document->id }}">
                    <button type="submit" class="btn btn-outline-success mt-2" style="margin-left: 5px;">Сделать
                        активным
                    </button>
                </form>
            @endif

        </div>

        <div class="d-flex mt-2">
            <iframe src="{{ route('documents.render', ['pdf' => $document->file]) }}"
                    style="width:50%;height:800px"></iframe>
            <div class="col-6" style="margin-left: 20px;">
                <h4>Информация</h4>
                <div class="data">
                    <p>Автор: {{ $document->documentable->responsibleUser->name }}</p>
                    <p>Дата создания: {{ $document->created_at }}</p>
                    <p>Источник: <a
                            href="{{ $document->documentable->sourceRoute }}">{{ $document->documentable->source->title }}</a>
                    </p>
                </div>
                <div class="comments">
                    <h4>Комментарии</h4>
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-8 col-lg-10">
                            <div class="card shadow-0 border" style="background-color: #f0f2f5;">
                                <div class="card-body p-4">
                                    <div class="form-outline mb-4">
                                        <form action="{{ route('comments.create') }}" method="POST">
                                            <div class="d-flex">
                                                @csrf
                                                <input type="text" id="addANote" class="form-control" name="text"
                                                       placeholder="Добавьте комментарий...">
                                                <input type="hidden" name="document_id" value="{{ $document->id }}">
                                                <button type="submit" class="btn btn-outline-info"
                                                        style="margin-left: 5px;">Добавить
                                                </button>
                                            </div>
                                        </form>

                                        <div class="form-notch">
                                            <div class="form-notch-leading" style="width: 9px;">
                                            </div>
                                            <div class="form-notch-middle" style="width: 80px;">
                                            </div>
                                            <div class="form-notch-trailing">
                                            </div>
                                        </div>
                                    </div>

                                    @foreach($document->comments as $comment)
                                        <div class="card mt-2">
                                            <div class="card-body">
                                                <p class="small"><span><em>{{ $comment->created_at }}</em></span></p>
                                                <p>{{ $comment->text }}</p>

                                                <div class="d-flex justify-content-between">
                                                    <div class="d-flex flex-row align-items-center">
                                                        <p class="small mb-0 ms-2">
                                                            <span>{{ $comment->user->role->name }} </span>{{ $comment->user->name }}
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                @if($document->active)
                    <div class="approved mt-3">
                        <h4>Подтверждение</h4>
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-12 col-lg-12">
                                <div class="card shadow-0 border" style="background-color: #f0f2f5;">
                                    <div class="card-body p-4">
                                        <div class="form-outline mb-4">
                                            <form action="{{ route('document.approve.create') }}" method="POST">
                                                <div class="d-flex">
                                                    @csrf
                                                    <input type="hidden" value="{{ $document->id }}" name="documentId">
                                                    <select class="form-select" aria-label="Default select example"
                                                            name="userId">
                                                        <option value="0">
                                                            Выберите
                                                        </option>
                                                        @foreach($users as $user)
                                                            <option value="{{ $user->id }}">
                                                                {{ $user->name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    <button type="submit" class="btn btn-outline-info"
                                                            style="margin-left: 5px;">Добавить
                                                    </button>
                                                </div>
                                            </form>

                                            <div>
                                                @foreach($document->approves as $approve)
                                                    <div class="d-flex align-items-center mt-2">
                                                        <div class="ruling d-flex">
                                                            @if($approve->approved !== \App\Models\Enums\ApproveStatusesEnum::YES)
                                                                <form action="{{ route('document.approve.delete') }}"
                                                                      method="POST">
                                                                    @method('DELETE')
                                                                    @csrf
                                                                    <input type="hidden" name="approveId"
                                                                           value="{{ $approve->id }}">
                                                                    <button type="submit"
                                                                            class="btn btn-danger fs-7 bi-trash3"></button>
                                                                </form>
                                                                @if(auth()->user()->id === $approve->user_id)
                                                                    <form style="margin-left: 4px;"
                                                                          action="{{ route('document.approve.status', ['status' => \App\Models\Enums\ApproveStatusesEnum::YES]) }}"
                                                                          method="POST">
                                                                        @csrf
                                                                        <input type="hidden" name="approveId"
                                                                               value="{{ $approve->id }}">
                                                                        <button type="submit"
                                                                                class="btn btn-success fs-7 bi-check-circle"></button>
                                                                    </form>
                                                                    <form style="margin-left: 5px;"
                                                                          action="{{ route('document.approve.status', ['status' => \App\Models\Enums\ApproveStatusesEnum::NO]) }}"
                                                                          method="POST">
                                                                        @csrf
                                                                        <input type="hidden" name="approveId"
                                                                               value="{{ $approve->id }}">
                                                                        <button type="submit"
                                                                                class="btn btn-warning fs-7 bi-x-circle"></button>
                                                                    </form>
                                                                @endif
                                                            @endif
                                                        </div>

                                                        <div style="margin-left: 5px;">{{ $approve->user->name }}:
                                                            <b>{{ \App\Models\Enums\ApproveStatusesEnum::STATUSES[$approve->approved]}}</b>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    @endforeach

@endsection
