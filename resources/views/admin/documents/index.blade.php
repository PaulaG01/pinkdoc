@extends('layouts.app')

@section('content')
    <hr>
{{--    @if(request()->route()->getName() === 'documents.all')--}}
{{--        <a href="{{ route('bids.createForm') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">--}}
{{--            <button type="button" class="btn btn-outline-success">Создать</button>--}}
{{--        </a>--}}
{{--        <hr>--}}
{{--    @endif--}}
    <form action="{{ route('documents.all') }}" method="GET">
        <div class="mb-2 row">
            <div class="col-sm-2">
                <input type="text" class="form-control" id="search" name="search" placeholder="Введите для поиска" value="{{ request('search') ?? '' }}">
            </div>
{{--            @if(request()->route()->getName() === 'documents.all')--}}
{{--                <select class="form-select col-sm-2" id="responsible" name="responsible"--}}
{{--                        style="width: auto; margin-right: 10px;">--}}
{{--                    <option value="0">--}}
{{--                        Выберите менеджера--}}
{{--                    </option>--}}
{{--                    @foreach($managers as $manager)--}}
{{--                        <option value="{{ $manager->id }}" @if(request('responsible') == $manager->id) selected @endif>--}}
{{--                            {{ $manager->name }}--}}
{{--                        </option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--            @endif--}}
            <button type="submit" class="col-sm-1 btn btn-success">Поиск</button>
        </div>
    </form>

    <form action="{{ route('documents.all') }}" method="GET">
        <button type="submit" class="col-sm-1 btn btn-warning">Очистить</button>
    </form>

    <table class="table mt-3">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Шаблон</th>
            <th scope="col">Дата создания</th>
            <th scope="col">Автор</th>
            <th scope="col">Кол-во версий</th>
            <th scope="col">История</th>
            <th scope="col">Одобрено</th>
        </tr>
        </thead>
        <tbody>
        @foreach($documents as $document)
            <tr>
                <td>{{ $document->id }}</td>
                <td>{{ $document->template }}</td>
                <td>{{ $document->created_at }}</td>
                <td>{{ $document->documentable->responsibleUser->name }}</td>
                <td>{{ $document->documentable->versionsCount }}</td>
                <td><a href="{{ route('documents.show', ['id' => $document->id]) }}">История</a></td>
                <td>{{ $document->isApproved ? 'Да' : 'Нет' }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
