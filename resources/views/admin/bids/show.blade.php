@extends('layouts.app')

@section('content')
    <h4 class="mt-3">Данные заявки</h4>
    <hr>
    <form action="{{ route('bids.update') }}" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{ $bid->id }}">
        <div class="mb-3 row">
            <label for="email" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="email" name="email" value="{{ $bid->email }}">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="phone" class="col-sm-2 col-form-label">Телефон</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="phone" name="phone" value="{{ $bid->phone }}">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="owner" class="col-sm-2 col-form-label">Контактное лицо</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="owner" name="owner" value="{{ $bid->owner }}">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="text" class="col-sm-2 col-form-label">Текст запроса</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="text" name="text" value="{{ $bid->text }}">
            </div>
        </div>
        <div class="mb-3 row">
            <label for="created_at" class="col-sm-2 col-form-label">Дата поступления</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="created_at" name="created_at" value="{{ $bid->created_at }}"
                       disabled>
            </div>
        </div>
        <h4 class="mt-3">Обработка заявки</h4>
        <hr>
        <div class="mb-3 row">
            <label for="responsible" class="col-sm-2 col-form-label">Ответственный за обработку менеджер</label>
            <div class="col-sm-10">
                <select class="form-select" aria-label="Default select example" id="responsible" name="responsible"
                        @if($bid->approved) disabled @endif>
                    <option value="0" @if(!$bid->responsible) selected @endif>
                        -
                    </option>
                    @foreach($users as $user)
                        <option value="{{ $user->id }}" @if($bid->responsible === $user->id) selected @endif>
                            {{ $user->name }}
                        </option>
                    @endforeach
                </select>
                @if(!$bid->responsible)
                    <p style="color: #bb2d3b">Требуется назначение ответственного менеджера. После назначения обработки
                        запроса менеджер получит уведомление о новой задаче.</p>
                @endif
            </div>

        </div>
        <div class="mb-3 row">
            <div class="col">
                <label for="due_date" class="col col-form-label">Срок обработки запроса (вычисляется с момента принятия
                    к исполнению)
                    @if($bid->done)
                        <span style="color: green">(Обработано)</span>
                    @elseif($bid->approved)
                        @if($bid->daysToPostponed > 0)
                            (Осталось дней: {{ $bid->daysToPostponed }})
                        @else
                            <span style="color: red">(Просрочено)</span>
                        @endif
                    @endif

                </label>
                <div class="col">
                    <input type="text" class="form-control" id="due_date" name="due_date" value="{{ $bid->due_date }}"
                           disabled>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="form-check">
                    @if($bid->approved)
                        <p>Принято к исполнению: {{ $bid->approved_date }}</p>
                    @else
                        <p style="color: #bb2d3b">Заявка еще не принята к исполнению. В случае возникновения вопросов
                            свяжитесь с ответственным лицом</p>
                    @endif
                    @if(auth()->user()->id === $bid->responsible)
                        <input class="form-check-input" type="checkbox" value="{{ $bid->approved }}" id="approved"
                               name="approved[]" @if($bid->approved) checked disabled @endif>
                        <label class="form-check-label" for="approved">
                            Заявка принята к исполению
                        </label>
                        @if(!$bid->approved)
                            <p><em>Принимая зявку к исполнению, вы обязуетесь уточнить все требования по мероприятияю у
                                    заказчика и составить документ. Срок обработки заявки: 7 календарных дней.</em></p>
                        @endif
                    @endif
                </div>
            </div>
            @if($bid->approved)
                <div class="col">
                    <div class="form-check">
                        @if($bid->done)
                            <p>Обработано: {{ $bid->done_date }}</p>
                        @else
                            <p>Заявка еще не обработана</p>
                        @endif
                        @if(auth()->user()->id === $bid->responsible)
                            <input class="form-check-input" type="checkbox" value="{{ $bid->done }}" id="done"
                                   name="done[]"
                                   @if($bid->done) checked disabled @endif>
                            <label class="form-check-label" for="done">
                                Заявка обработана
                            </label>
                            @if(!$bid->done)
                                <p><em>Подтверждая окончание обработки заявки, вы гарантируете, что документ составлен
                                        корректно.</em></p>
                            @endif
                        @endif
                    </div>
                </div>
            @endif
        </div>
        <button type="submit" class="btn btn-success mt-3"
                @if(auth()->user()->id !== $bid->responsible && $bid->approved) disabled @endif>Сохранить
        </button>
    </form>
    @if($bid->document)
        <h4 class="mt-3">Оформление документа</h4>
        <hr>
        @if($bid->document->documents->count())
            <div class="alert alert-warning" role="alert">
                На основе заявки уже были созданы документы. (Актуальная версия:
                <a href="{{ route('documents.show', ['id' => $bid->document->documents[0]]) }}">
                    №{{ $bid->document->documents[0]->id }}
                </a>)
                Последующие изменения приведут к созданию новой версии документа.
            </div>
        @endif
        <form action="{{ route('bid.document.update') }}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $bid->document->id }}">
            <div class="mb-3 row">
                <label for="company" class="col-sm-2 col-form-label">Имя компании\физ.лица</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="company" name="company"
                           value="{{ $bid->document->company }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="event_type" class="col-sm-2 col-form-label">Тип мероприятия</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="event_type" name="event_type"
                           value="{{ $bid->document->event_type }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="address" class="col-sm-2 col-form-label">Место проведения</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="address" name="address"
                           value="{{ $bid->document->address }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="start_date" class="col-sm-2 col-form-label">Дата начала</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="start_date" name="start_date"
                           value="{{ $bid->document->start_date }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="end_date" class="col-sm-2 col-form-label">Дата окончания</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="end_date" name="end_date"
                           value="{{ $bid->document->end_date }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="guests_count" class="col-sm-2 col-form-label">Кол-во гостей</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="guests_count" name="guests_count"
                           value="{{ $bid->document->guests_count }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label for="budget" class="col-sm-2 col-form-label">Бютджет мероприятия, $</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="budget" name="budget"
                           value="{{ $bid->document->budget }}">
                </div>
            </div>
            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Утвержденные пункты:</label>
                <div class="col-sm-10" id="bidDocumentItems">
                    @foreach($bid->document->items as $item)
                        <input type="text" class="form-control mt-2" name="items[{{ $item->id }}]"
                               value="{{ $item->text }}">
                    @endforeach
                    <div class="btn btn-outline-success mt-2" onclick="addInput()">Добавить пункт</div>
                </div>
            </div>
            <button type="submit" class="btn btn-success mt-3"
                    @if(auth()->user()->id !== $bid->responsible) disabled @endif>Сохранить
            </button>
        </form>
    @endif


@endsection
