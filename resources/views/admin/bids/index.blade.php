@extends('layouts.app')

@section('content')
    <hr>
    @if(request()->route()->getName() === 'bids.all')
        <a href="{{ route('bids.createForm') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">
            <button type="button" class="btn btn-outline-success">Создать</button>
        </a>
        <hr>
    @endif
    <form action="{{ route('bids.all') }}" method="GET">
        <div class="mb-2 row">
            <div class="col-sm-2">
                <input type="text" class="form-control" id="search" name="search" placeholder="Введите для поиска" value="{{ request('search') ?? '' }}">
            </div>
            @if(request()->route()->getName() === 'bids.all')
                <select class="form-select col-sm-2" id="responsible" name="responsible"
                        style="width: auto; margin-right: 10px;">
                    <option value="0">
                        Выберите менеджера
                    </option>
                    @foreach($managers as $manager)
                        <option value="{{ $manager->id }}" @if(request('responsible') == $manager->id) selected @endif>
                            {{ $manager->name }}
                        </option>
                    @endforeach
                </select>
            @endif
            <button type="submit" class="col-sm-1 btn btn-success">Поиск</button>
        </div>
    </form>

    <form action="{{ route('bids.all') }}" method="GET">
        <button type="submit" class="col-sm-1 btn btn-warning">Очистить</button>
    </form>

    <table class="table mt-3">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Заказчик</th>
            <th scope="col">Заголовок</th>
            <th scope="col">Дата создания</th>
            <th scope="col">Ответственный</th>
            <th scope="col">Принято к исполениню</th>
            <th scope="col">Срок обработки</th>
            <th scope="col">Обработано</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($bids as $bid)
            <tr @if($bid->approved && !$bid->done) style="background-color: lightblue;"
                @elseif($bid->approved && $bid->done) style="background-color: darkseagreen;"@endif>
                <th scope="row">{{ $bid->id }}</th>
                <td>{{ $bid->owner }}</td>
                <td>
                    <a href="{{ route('bids.show', ['id' => $bid->id]) }}">{{ $bid->title }}</a>
                </td>
                <td>{{ $bid->created_at }}</td>
                <td>{{ $bid->responsibleUser ? $bid->responsibleUser->name : '-' }}</td>
                <td>{{ $bid->approved ? 'Да' : 'Нет' }}</td>
                <td>{{ $bid->due_date ?? '-' }}</td>
                <td>{{ $bid->done ? 'Да' : 'Нет' }}</td>
                <td>
                    <form action="" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger">Удалить
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
