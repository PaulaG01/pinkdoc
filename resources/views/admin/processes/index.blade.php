@extends('layouts.app')

@section('content')
    @foreach($bids as $bid)
        <div class="row mt-3">
            <div class="d-flex align-items-center">
                <div class="bid">
                    <div class="card
                    @if(!$bid->approved) border-danger
                    @elseif(!$bid->done) border-warning
                    @elseif($bid->done) border-success
                    @endif" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title"><a
                                    href="{{ route('bids.show', ['id'=> $bid->id]) }}">{{ $bid->title }}</a></h5>
                            <h6 class="card-subtitle mb-2 text-muted">{{ $bid->owner }}</h6>
                            <p class="card-text">{{ $bid->text }}</p>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">{{ $bid->responsibleUser ? $bid->responsibleUser->name : '-' }}</li>
                        </ul>
                    </div>
                </div>
                @if($bid->document)
                    --------
                    <div class="bid-document">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">{{ $bid->document->company }}</h5>
                                <p class="card-text">{{ $bid->document->address }}, {{ $bid->document->start_date }}
                                    - {{ $bid->document->end_date }}</p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Бютджет: {{ $bid->document->budget }}</li>
                                <li class="list-group-item">Гости: {{ $bid->document->guests_count }}</li>
                                @foreach($bid->document->items as $item)
                                    <li class="list-group-item">{{ $item->text }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @if($bid->document->documents->count())
                        --------
                        <div class="document">
                            <div class="card text-center">
                                <div class="card-header">
                                    <ul class="nav nav-pills card-header-pills">
                                        @foreach($bid->document->documents as $document)
                                            <li class="nav-item">
                                                <a class="nav-link @if($document->active) active @endif"
                                                   href="{{ route('documents.show', ['id' => $document->id]) }}">№{{ $document->id }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="card-body text-lg-start">
                                    <p class="card-text">Дата создания: <b>{{ $document->created_at }}</b></p>
                                    <p class="card-text">Комментарии: <b>{{ $document->comments->count() }}</b></p>
                                    <p class="card-text">Утверждено: <b>{{ $document->approves->where('approved', 1)->count() }}</b> из <b>{{ $document->approves->count() }}</b></p>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif

            </div>
        </div>
    @endforeach
@endsection
