@extends('layouts.app')

@section('content')
    <hr>
    <a href="{{ route('users.registerForm') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">
        <button type="button" class="btn btn-outline-success">Создать</button>
    </a>

    <hr>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ФИО</th>
            <th scope="col">Email</th>
            <th scope="col">Зарегистрирован</th>
            <th scope="col">Должность</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{ $user->id }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->role->name }}</td>
                <td>
                    <form action="{{ route('users.delete', $user->id) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="btn btn-danger"
                                @if(auth()->user()->id === $user->id) disabled @endif>Удалить
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
