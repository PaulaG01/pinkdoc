<?php

use App\Models\DTO\PDF\BidDocumentDTO;

$dto = (object)$dto;

/** @var BidDocumentDTO $dto */
?>

    <!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style type="text/css">
        * {

            font-family: "DejaVu Sans", serif;
            font-size: 12px;
        }

        .under {
            border-bottom: 1px solid gray;
            padding-bottom: 2px;
            margin-right: 5px;
            margin-left: 5px;
            padding-left: 10px;
            padding-right: 10px
        }

        .under-super {
            border-bottom: 1px solid gray;
            padding-bottom: 2px;
            margin-right: 10px;
            margin-left: 10px;
            padding-left: 30px;
            padding-right: 30px;
        }
    </style>
</head>
<body>
<div class="">
    <p>
        Заявление №
        <span class="under">{{ $dto->id }}</span><br>
    </p>
</div>
<div class="" style="text-align: right">
    <p>
        Генеральному директору
        <span class="under">{{ $dto->ownerCompanyTitle }}</span>,<br>
        <span class="under">{{ $dto->ownerCompanyAddress }}</span>, <br>
        <span class="under">{{ $dto->ownerCompanyDirector }}</span>
    </p>
</div>

<div style="text-align: center; margin: 0 20px">Заявка на организацию мероприятия</div>

<p>
    Компания
    <span class="under">{{ $dto->company }}</span>
    просит рассмотреть возможность
    организации официального мероприятия
    в период с
    <span class="under">{{ $dto->dateStart }}</span>
    по
    <span class="under">{{ $dto->dateEnd }}</span>
    по адресу
    <span class="under">{{ $dto->address }}</span>.
    Вид мероприятия
    <span class="under">{{ $dto->type }}</span>,
    предполагаемое количество человек
    <span class="under">{{ $dto->guestsCount }}</span>.
    Выделенный бюджет составляет
    <span class="under">{{ $dto->budget }}</span> $.
</p>

<div style="margin: 0 20px">
    <p>Утвержденные пункты на исполнение:</p>
    @foreach($dto->items as $i => $items)
        @foreach($items as $index => $item)
            <p>{{ $index + 1 }}. <span class="under-super"> {{ $item->text }}</span></p>
        @endforeach
        @break
    @endforeach
</div>
</body>
</html>

