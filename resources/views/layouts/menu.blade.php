<div class="container-fluid">
    <div class="row flex-nowrap">
        <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
            <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">
                <span class="fs-5 d-none d-sm-inline">Меню</span>
                @if(auth()->user()->role_id !== \App\Models\Enums\RolesEnum::CLIENT)
                <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                    <li style="width: 100%">
                        <div style="width: 100%; height: 20px; border-bottom: 1px solid black; text-align: left">
                          <span style="background-color: #212529; padding: 3px 10px;">
                            Компания
                          </span>
                        </div>
                    </li>

                    <li>
                        <a href="{{ route('users.all') }}" class="nav-link px-0 align-middle">
                            <i class="fs-4 bi-people"></i> <span class="ms-1 d-none d-sm-inline">Пользователи</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('bids.all') }}" class="nav-link px-0 align-middle">
                            <i class="fs-4 bi-envelope-paper"></i>
                            <span class="ms-1 d-none d-sm-inline">Пользовательские заявки</span>
                            @if(\App\Services\BidsService::getBidsCount('new'))
                                <span class="badge rounded-pill bg-danger">
                                {{ \App\Services\BidsService::getBidsCount('new') }}
                                </span>
                            @endif
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('documents.all') }}" class="nav-link px-0 align-middle">
                            <i class="fs-4 bi-file-earmark"></i> <span class="ms-1 d-none d-sm-inline">Документы</span> </a>
                    </li>
                    <li>
                        <a href="{{ route('processes.all') }}" class="nav-link px-0 align-middle">
                            <i class="fs-4 bi-cpu"></i> <span class="ms-1 d-none d-sm-inline">Процессы</span> </a>
                    </li>

                    <li style="width: 100%">
                        <div style="width: 100%; height: 20px; border-bottom: 1px solid black; text-align: left">
                          <span style="background-color: #212529; padding: 3px 10px;">
                            Мои документы
                          </span>
                        </div>
                    </li>

                    <li>
                        <a href="{{ route('bids.my') }}" class="nav-link px-0 align-middle">
                            <i class="fs-4 bi-envelope-paper"></i> <span class="ms-1 d-none d-sm-inline">Заявки</span> </a>
                    </li>
                    <li>
                        <a href="#" class="nav-link px-0 align-middle">
                            <i class="fs-4 bi-file-earmark"></i> <span class="ms-1 d-none d-sm-inline">Документы</span> </a>
                    </li>
                </ul>
                @endif

                <hr>
            </div>
        </div>
        <div class="col py-3">
            <h3> {{ $pageTitle ?? 'PinkDoc' }}</h3>
            <div>
                @yield('content')
            </div>
        </div>
    </div>
</div>
