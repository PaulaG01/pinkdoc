<?php

use App\Http\Controllers\Admin\BidDocumentsController;
use App\Http\Controllers\Admin\BidsController;
use App\Http\Controllers\Admin\CommentsController;
use App\Http\Controllers\Admin\DocumentApproversController;
use App\Http\Controllers\Admin\DocumentsController;
use App\Http\Controllers\Admin\ProcessesController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Pages\PagesController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('admin');

Route::prefix('users')->group(function () {
    Route::post('/create', [UsersController::class, 'create'])->name('site.users.create');
});

Route::prefix('pages')->group(function (){
    Route::get('/bids', [PagesController::class, 'bids'])->name('pages.bids');
    Route::post('/bids/create', [BidsController::class, 'create'])->name('site.bids.create');
});

Route::middleware('auth')->prefix('admin')->group(function () {

    Route::get('/users-register-form', [UsersController::class, 'showRegistrationForm'])->name('users.registerForm');
    Route::get('/bids-create-form', [BidsController::class, 'showCreateForm'])->name('bids.createForm');

    Route::prefix('users')->group(function () {
        Route::get('/all', [UsersController::class, 'index'])->name('users.all');
        Route::post('/create', [UsersController::class, 'create'])->name('users.create');
        Route::delete('/delete/{userId}', [UsersController::class, 'destroy'])->name('users.delete');
    });

    Route::prefix('bids')->group(function () {
        Route::get('/all', [BidsController::class, 'index'])->name('bids.all');
        Route::get('/my', [BidsController::class, 'my'])->name('bids.my');
        Route::get('/show/{id}', [BidsController::class, 'show'])->name('bids.show');
        Route::post('/update', [BidsController::class, 'update'])->name('bids.update');
        Route::post('/create', [BidsController::class, 'create'])->name('bids.create');
    });

    Route::prefix('bid-documents')->group(function () {
        Route::post('/update', [BidDocumentsController::class, 'update'])->name('bid.document.update');
    });


    Route::prefix('documents')->group(function () {
        Route::get('/all', [DocumentsController::class, 'index'])->name('documents.all');
        Route::get('/show/{id}', [DocumentsController::class, 'show'])->name('documents.show');
        Route::get('/render', [DocumentsController::class, 'renderPdf'])->name('documents.render');
        Route::post('/set-active', [DocumentsController::class, 'setActive'])->name('documents.setActive');
    });

    Route::prefix('document-approves')->group(function () {
        Route::post('/create', [DocumentApproversController::class, 'create'])->name('document.approve.create');
        Route::delete('/delete', [DocumentApproversController::class, 'delete'])->name('document.approve.delete');
        Route::post('/status', [DocumentApproversController::class, 'changeStatus'])->name('document.approve.status');
    });

    Route::prefix('comments')->group(function () {
            Route::post('/create', [CommentsController::class, 'create'])->name('comments.create');
    });

    Route::prefix('processes')->group(function () {
        Route::get('/all', [ProcessesController::class, 'index'])->name('processes.all');
    });

});
